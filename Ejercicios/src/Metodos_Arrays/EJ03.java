/**
 * 
 */
package Metodos_Arrays;
import javax.swing.JOptionPane;
/**
 * @author YASIN
 * @version 1.0
 * @date 03/06/2016
 *
 */
public class EJ03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String t=JOptionPane.showInputDialog("Introduce numero");
		int numero=Integer.parseInt(t);
		
		if (esPrimo(numero)){
			JOptionPane.showMessageDialog(null, "El numero "+numero+" es primo");
		}else{
			JOptionPane.showMessageDialog(null, "El numero "+numero+" no es primo");
		}
	}
	public static boolean esPrimo (int numero){
		if (numero<=1){
			return false;
		}
		
		int cont=0;
		for (int divisor=(int)Math.sqrt(numero);divisor>1;divisor--){
			if (numero%divisor==0){
				cont+=1;
			}
		}
		
		if (cont>=1){
			return false;
		}else{
			return true;
		}
	}
}
		