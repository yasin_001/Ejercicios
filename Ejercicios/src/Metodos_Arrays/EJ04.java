/**
 * 
 */
package Metodos_Arrays;
import javax.swing.JOptionPane;
/**
 * @author Yasin
 * @version 1.0
 * @date 03./06/2016
 *
 */
public class EJ04 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String t=JOptionPane.showInputDialog("Introduce un numero");
		int numero=Integer.parseInt(t);
		JOptionPane.showMessageDialog(null, "El factorial de "+numero+" es "+factorial(numero));

	}

	public static int factorial (int numero) {
		// TODO Auto-generated method stub
		 int resultado=numero;
		 for(int cont=(numero-1);cont>0;cont--){
			 resultado=resultado*cont;
		 }
		 return resultado;
	}

}
