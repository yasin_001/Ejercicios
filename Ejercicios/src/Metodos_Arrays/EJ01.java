/**
 * 
 */
package Metodos_Arrays;
import javax.swing.JOptionPane;
/**
 * @author Yasin
 * @version 1.0
 * @date 002/06/2016
 *
 */
public class EJ01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String t="";
		
		double resultado=0;
		String opcion=JOptionPane.showInputDialog("Elige que figura deseas calcular: circulo, triangulo o cuadrado");
		switch (opcion){
		case "circulo":
			 t=JOptionPane.showInputDialog("Introduce el radio");
	         int radio=Integer.parseInt(t);
	         resultado=aCirculo(radio);
	         break;
	            
		case "triangulo":
			t=JOptionPane.showInputDialog("Introduce la base");
            int base=Integer.parseInt(t);
            t=JOptionPane.showInputDialog("Introduce la altura");
            int altura=Integer.parseInt(t);
            resultado=aTriangulo(base, altura);
            break;
	            
		case "cuadrado":
			 t=JOptionPane.showInputDialog("Introduce la medida de un lado");
	         int lado=Integer.parseInt(t);
	         resultado=aCuadrado(lado);
	         break;
	        
		default:
            System.out.println("No has introducido una figura correcta. (Ej: circulo, triangulo o cuadrado)");
		}
		System.out.println("El area del "+opcion+" es "+resultado);
	}
	
	 public static double aCirculo (int radio){
	        return Math.pow(radio, 2)*Math.PI;
	    }
	   public static double aTriangulo (int base, int altura){
	        return base*altura/2;
	    }
	   public static double aCuadrado (int lado){
	        return lado*lado;
	    }
}
